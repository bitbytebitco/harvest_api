# Description 
This is a small library that I wrote to connect the [BitByteBit](https://bitbytebit.co) business website  to my Harvest Time Tracking account. 
Harvest is what I use to handle some of the business administration for BitByteBit (invoicing, estimating, etc). 
This library helps support the client portal of [BitByteBit](https://bitbytebit.co) so that clients may view estimates, invoices, and make payments.


### Example Usage:

```python
from harvest_api import client
projects = client.get_projects_by_client_id(<HARVEST_CLIENT_ID>)
```

