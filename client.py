#!/usr/bin/python

import os
import json
import datetime
import requests 
import urllib.parse 
from configparser import SafeConfigParser

parser = SafeConfigParser()
parser.read('/home/beckerz/configs/bitbytebit.ini')

api_host = "https://api.harvestapp.com/"
api_urls = {
    'me': "/v2/users/me",
    'clients': "/v2/clients/",
    'invoices': "/v2/invoices/",
    'invoice_payments': "/v2/invoices/%s/payments",
    'projects': "/v2/projects/",
    'estimates': "/v2/estimates/",
    'contacts': "/v2/contacts/",
    'time_entries': "/v2/time_entries/",
    'project_assignments': "/v2/projects/%s/task_assignments",
}

access_token = parser.get("harvest","HARVEST_ACCESS_TOKEN")
account_id = parser.get("harvest","HARVEST_ACCOUNT_ID")

headers = {
    "User-Agent": "BitByteBitAccessToken (bitbytebitco@gmail.com)",
    "Authorization": "Bearer %s" % access_token,
    "Harvest-Account-ID": account_id,
}

def api_request(route, *args, **kwargs):
    try:
        if len(args)>0:
            url = urllib.parse.urljoin(api_host, "%s%s" % (route, args[0]))
        else:
            url = urllib.parse.urljoin(api_host, route)
        if 'method' in kwargs and 'data' in kwargs: 
            if kwargs['method'] == 'post':
                r = requests.post(url, headers=headers, data=kwargs['data'])
            elif kwargs['method'] == 'patch':
                r = requests.patch(url, headers=headers, data=kwargs['data'])
        else:
            r = requests.get(url, headers=headers)
        if r.status_code == 200 or r.status_code == 201:
            return r.content
    except Exception as e:
        print('** Harvest API : api_request error')
        print(e)
        return None 

def get_clients():
    req = api_request(api_urls['clients'])
    if req is not None:
        return json.loads(req)['clients']        

def get_client(client_id):
    req = api_request(api_urls['clients'], client_id)
    if req is not None:
        return json.loads(req.decode('utf-8'))

def get_contacts():
    req = api_request(api_urls['contacts'])
    if req is not None:
        return json.loads(req.decode('utf-8'))

def get_contact_by_email(email):
    ret = get_contacts()
    if len(ret): 
        for con in ret['contacts']:
            if con['email'] == email:
                return con

def get_contact_by_id(harvest_id):
    ret = get_contacts()
    if len(ret):
        for con in ret['contacts']:
            print(con['client']['id'])
            if int(con['client']['id']) == int(harvest_id):
                return con

def update_client_by_id(client_id, data):
    req = api_request(api_urls['clients'], client_id, method="patch", data=data)
    if req is not None:
        return json.loads(req)

def update_contact_by_id(contact_id, data):
    req = api_request(api_urls['contacts'], contact_id, method="patch", data=data)
    if req is not None:
        return json.loads(req)

def get_invoices():
    req = api_request(api_urls['invoices'])
    if req is not None:
        return json.loads(req)

def get_invoice_by_id(invoice_id):
    req = api_request(api_urls['invoices']+str(invoice_id))
    if req is not None:
        return json.loads(req.decode('utf-8'))

def create_invoice_payment(invoice_id, data):
    req = api_request(api_urls['invoice_payments'] % invoice_id, method="post", data=data)
    if req is not None:
        return json.loads(req.decode('utf-8'))

def get_invoices_by_client_id(client_id):
    req = api_request(api_urls['invoices']+ "?client_id=%s"%client_id)
    if req is not None:
        return json.loads(req.decode('utf-8'))['invoices']

def get_active_projects(is_active=True):
    req = api_request(api_urls['projects']+ "?is_active=%s"%is_active)
    if req is not None:
        return json.loads(req.decode('utf-8'))['projects']

def get_projects_by_client_id(client_id):
    req = api_request(api_urls['projects']+ "?client_id=%s"%client_id)
    if req is not None:
        print(req)
        return json.loads(req.decode('utf-8'))['projects']

def get_project_by_id(project_id):
    req = api_request(api_urls['projects']+ str(project_id))
    if req is not None:
        return json.loads(req)

def get_project_tasks_by_id(project_id):
    req = api_request(api_urls['project_assignments'] % project_id)
    if req is not None:
        return json.loads(req)

def get_estimates_by_client_id(client_id):
    req = api_request(api_urls['estimates']+ "?client_id="+client_id)
    if req is not None:
        return json.loads(req.decode('utf-8'))['estimates']

# TIME ENTRIES
def get_time_entries_by_project_id(project_id):
    print("project_id: %s" % project_id)
    req = api_request(api_urls['time_entries']+ "?project_id=%s" % project_id)
    if req is not None:
        return json.loads(req)['time_entries']

def get_hourly_total_by_task_id(task_id, project_id=None):
    print("task_id: %s" % task_id)
    time_entries = get_time_entries_by_project_id(project_id)
    total_hours = 0
    for entry in time_entries:
        #print str(entry['task']['id'])
        #print task_id
        if str(entry['task']['id']) == str(task_id):
            total_hours = total_hours + entry['hours']
    return total_hours 

def get_project_hours_to_date_by_project_id(project_id):
    time_entries = get_time_entries_by_project_id(project_id)
    total_hours = 0
    for entries in time_entries:
        total_hours = total_hours + entries['hours']
    return total_hours

if __name__ == "__main__":
    print('main')
    #print json.dumps(get_projects_by_client_id('6812078'))
    #print json.dumps(get_project_by_id('17485540'))
    #print json.dumps(get_project_tasks_by_id('17485540'))
    #print json.dumps(get_hourly_total_by_task_id('175744035',project_id='16225441'))
    #print get_contact_by_email("contact@acorninn.com")
    #print get_project_hours_to_date_by_project_id('16260673')
